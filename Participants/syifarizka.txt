Syifa Rizka
syifarizka23

https://gitlab.com/syifarizka23/ddppw

Checklist Week00

GIT - PRIBADI
[x] Membuat repositori Git di lokal dengan menggunakan git init
[x] Membuat suatu file dan menambahkannya ke dalam staging repositori
[x] Melakukan commit terhadap perubahan di staging dengan git commit
[x] Menambahkan remote baru ke repositori lokal dengan git init
[x] Melakukan push ke GitLab
[x] Membuat satu branch di repositori dan push branch tersebut dengan isi 1 file
[x] Membuat merge request ke branch master dan melakukan merge
[x] Mengetahui cara menggali informasi tentang commit melalui git log
[x] Mengganti level akses GitLab ke Public

GIT - BERSAMA
[x] Melakukan git init, git remote add, dan git pull terhadap repositori ini (https://gitlab.com/skycruiser8/ddppw)
[x] Membuat branch baru dengan nama yang unik (boleh nama sendiri, nama mantan, dll.)
[x] Membuat berkas <username gitlab>.txt di folder Participants dengan konten seperti berkas ini
[x] Push branch masing-masing ke GitLab
[x] Membuat merge request ke branch master

VIRTUALENV
[ ] Membuat virtual environment baru di repositori DDPPW masing-masing
[ ] Menambahkan folder virtual environment ke .gitignore
[ ] Memastikan bahwa virtual environment tidak turut terdorong ke GitLab
[ ] Menjalankan virtual environment
[ ] Memasang Django di virtual environment
[ ] Memasang paket aplikasi DDPPW dengan menggunakan requirements.txt