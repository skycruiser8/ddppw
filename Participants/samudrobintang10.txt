Bintang Samudro
samudrobintang10

https://gitlab.com/samudrobintang10/DDPPW

Checklist Week00

GIT - PRIBADI
[V] Membuat repositori Git di lokal dengan menggunakan git init
[V] Membuat suatu file dan menambahkannya ke dalam staging repositori
[V] Melakukan commit terhadap perubahan di staging dengan git commit
[ ] Menambahkan remote baru ke repositori lokal dengan git init
[V] Melakukan push ke GitLab
[V] Membuat satu branch di repositori dan push branch tersebut dengan isi 1 file
[V] Membuat merge request ke branch master dan melakukan merge
[V] Mengetahui cara menggali informasi tentang commit melalui git log
[V] Mengganti level akses GitLab ke Public

GIT - BERSAMA
[V] Melakukan git init, git remote add, dan git pull terhadap repositori ini (https://gitlab.com/skycruiser8/ddppw)
[V] Membuat branch baru dengan nama yang unik (boleh nama sendiri, nama mantan, dll.)
[V] Membuat berkas <username gitlab>.txt di folder Participants dengan konten seperti berkas ini
[V] Push branch masing-masing ke GitLab
[V] Membuat merge request ke branch master

VIRTUALENV
[ ] Membuat virtual environment baru di repositori DDPPW masing-masing
[ ] Menambahkan folder virtual environment ke .gitignore
[ ] Memastikan bahwa virtual environment tidak turut terdorong ke GitLab
[ ] Menjalankan virtual environment
[ ] Memasang Django di virtual environment
[ ] Memasang paket aplikasi DDPPW dengan menggunakan requirements.txt
