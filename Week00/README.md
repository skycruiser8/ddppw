# Pekan 0: Pengenalan Perangkat Lunak

**DDPPW** | **Penulis**: Rafi Muhammad Daffa



## Daftar Isi

1. Daftar Perangkat Lunak
2. Git
3. GitLab
4. Virtualenv



## Daftar Perangkat Lunak

Dalam proses pengembangan situs web, ada dua jenis perangkat lunak yang terlibat, yaitu perangkat lunak dasar dan penunjang. Perangkat lunak dasar di sini mencakup bahasa pemrograman serta *framework* yang menjadi pondasi terhadap pembangunan situs web itu sendiri. Di sisi lain, perangkat lunak penunjang berfungsi untuk membantu pengguna dalam mengerjakan pembangunan situs web tersebut.

Pada materi ini, perangkat lunak yang akan diperkenalkan serta digunakan adalah sebagai berikut:

| Nama Perangkat Lunak | Jenis                      | Fungsi                                                       | Rekomendasi Versi                           |
| -------------------- | -------------------------- | ------------------------------------------------------------ | ------------------------------------------- |
| Python               | Dasar (Bahasa pemrograman) | Menjadi bahasa utama dalam pengembangan situs web pada materi ini | \>=3.6                                      |
| Django               | Dasar (*Framework*)        | Modul Python yang digunakan untuk mengembangkan situs web    | \>=3.0                                      |
| Git                  | Penunjang (VCS)            | Pengendali versi dokumen                                     | \>=2.20                                     |
| Virtualenv           | Penunjang (Lingkungan)     | Lingkungan Python yang terisolasi dari instalasi Python sistem dan memiliki struktur modul sendiri | Sesuai yang dipaketkan Python masing-masing |
| GitLab               | Penunjang (Layanan)        | Layanan untuk mengakomodasi repositori Git dan menyediakan fungsionalitas CI/CD | -                                           |
| Heroku               | Penunjang (Layanan)        | Layanan untuk *hosting* situs web yang bisa diakses oleh publik | -                                           |